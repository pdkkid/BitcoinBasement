﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ObjectPlacement : MonoBehaviour {

    //So we can check money and all that shit
    GameObject gameManager;


    //Setup for UIprefabs and such, also need the Canvas game object to instantate under..
    public GameObject Canvas;
    public GameObject[] UIPrefabs;
    private GameObject uiFollow;
    private GameObject objToFollow;

    GameObject[] spots;

    void Awake()
    {
        
        spots = GameObject.FindGameObjectsWithTag("BuildableSpot");
    }


    void Start()
    {
        gameManager = GameObject.Find("GameManager");
    }

    //Setup array of buildable items, named EXACTALY by their names to use for placment
    public GameObject[] buildItems;


    //use this when the shop button is pressed
    bool isInBuild = false;
    [Tooltip("Just for debug purposes")]
    public string currentItem;
    
    public void setCurrentBuildItem(string item)
    {
        currentItem = item;
    }

    //Test for putting ui on screen at a certain point... lets see how this goes..
    public void putUIonObject(Transform gameobj, RectTransform ui)
    {
        Vector2 screenPoint = RectTransformUtility.WorldToScreenPoint(Camera.main, gameobj.position);
        ui.anchoredPosition = screenPoint - Canvas.GetComponent<RectTransform>().sizeDelta / 2f + new Vector2(140, 140);
    }

    public void updateUI()
    {
        putUIonObject(objToFollow.transform, uiFollow.GetComponent<RectTransform>());
    }

    public void closeUI()
    {
        Destroy(uiFollow);
    }

    public void turnOffAllOthers(GameObject dontturnmeoff)
    {
        foreach(GameObject gam in spots)
        {
            gam.SetActive(false);
            dontturnmeoff.SetActive(true);
        }
    }


    //I wonder..
    public GameObject shelfprefab;
    
    public void isInBuildMode()
    {
        isInBuild = !isInBuild;
        Debug.Log("isInBuldmode used");
        //When in buildmode show all the spots you can place an item into
        Debug.Log(spots.Length);
        for(int i=0; i < spots.Length; i++)
        {
            Debug.Log("inside for loop with " + spots.Length);
            if (isInBuild)
            {
                spots[i].SetActive(true);
                closeUI();
            }
            if (!isInBuild)
            {
                spots[i].SetActive(false);
                closeUI();
            }
            
        }
    }

    void Update()
    {
        Debug.Log("update working lol");
        //For UI to follow gameobject
        if(uiFollow != null)
        {
            updateUI();
        }


        if (Input.GetMouseButtonDown(0))
        {
            RaycastHit hit;
            Ray ray = Camera.main.ScreenPointToRay(Input.mousePosition);
            if(Physics.Raycast (ray, out hit, 100.0f))
            {
                if (isInBuild)
                {
                    //Make sure whatever the raycast hit is a spot where we can put something
                    if (hit.transform.tag == "BuildableSpot")
                    {
                        string nameOfSpot = hit.transform.name;
                        Debug.Log("Spot hit name is " + nameOfSpot);

                        foreach(GameObject UI in UIPrefabs)
                        {
                            if(UI.name == nameOfSpot + "UI")
                            {
                                if(uiFollow != null)
                                {
                                    return;
                                }
                                uiFollow = Instantiate(UI, Canvas.transform);
                                objToFollow = hit.transform.gameObject;
                                turnOffAllOthers(hit.transform.gameObject);
                                
                            }
                            Debug.Log("Nothing found that matches what i should add here bruh..");

                        }


                      /*
                        Debug.Log("Current Item Selected = " + currentItem);
                        if(currentItem != null)
                        {
                            foreach(GameObject item in buildItems)
                            {
                                //Go though the array of buy-able objects and stop when we get to the one thats selected
                                if(item.transform.name == currentItem)
                                {
                                    //Check if there is enough BTC to buy whatever item is slected
                                    if(gameManager.GetComponent<StatsManager>().currentBTC > item.GetComponent<ObjectStats>().t_cost)
                                    {
                                        Instantiate(item, hit.transform.position, hit.transform.rotation);
                                        Destroy(hit.transform.gameObject);
                                        Debug.Log("Wow, how did we get here... you had enough btc to buy this");
                                    }
                                    else
                                    {
                                        Debug.Log("not enough BTC");
                                    }
                                }
                            }
                        }
                        */
                    }
                }
            }
        }
    }
}
