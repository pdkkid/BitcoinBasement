﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class StatsManager : MonoBehaviour {

    public Text money;
    public Text power;

    public int powerConsumption;
    public int powerMax;
    public float currentBTC;
    public float BTCPerSecond;

    //Setup for power overdraw
    public bool isOverdrawing;

	// Use this for initialization
	void Start () {
        //Setup the 1 second delay
        StartCoroutine(updateBTC());
        StartCoroutine(updatePower());
	}
	
	// Update is called once per frame
	void Update () {
	}

    //setup for invokeReapeating
    IEnumerator updateBTC()
    {
        while (true)
        {
            float increment = BTCPerSecond / 16;
            float newBTC = currentBTC + increment;
            money.text = (newBTC).ToString("0.000");
            currentBTC = newBTC;
            yield return new WaitForSeconds(0.0625f);
        }
    }

    IEnumerator updatePower()
    {
        while (true)
        {
            if(powerConsumption > powerMax)
            {
                isOverdrawing = true;
                power.color = Color.red;
                power.text = ("0 / " + powerMax);

            }
            power.text = (powerConsumption + " / " + powerMax);
            yield return new WaitForSeconds(1f);
        }
    }
}
